package fxlife

import java.io.File
import java.net.URLClassLoader

import org.scalatest.{FreeSpec, Matchers}

class FxLifeSpec extends FreeSpec with Matchers {
  "test" in {
    val url = new File("/home/adrien/Projects/sculpt/examples/life/build/classes/scala/main").toURI.toURL
    url.openConnection().setDefaultUseCaches(false)
    val classLoader = new URLClassLoader(Array(url))

    classLoader.loadClass("life.api.NeighborhoodSpec")
    classLoader.loadClass("fxlife.FxLife$$anon$2$$anon$3")
    classLoader.loadClass("sculpt.test.SculptRunner")
  }
}
