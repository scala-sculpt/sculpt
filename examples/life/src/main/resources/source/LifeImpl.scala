import life.api.{Cell, Life, Neighborhood, World}

class LifeImpl(worldFactory: World.Factory, neighborhood: Neighborhood.Factory) extends Life {

  def nextStep(cell: Cell, neighborCount: Int): Cell = cell match {
    case Cell.Live =>
      if(neighborCount == 2 || neighborCount == 3) Cell.Live else Cell.Dead
    case Cell.Dead =>
      if(neighborCount == 3) Cell.Live else Cell.Dead
  }

  def nextStep(world: World): World = {
    val positions = for {
      x <- 0 until world.width
      y <- 0 until world.height
    } yield (x, y)

    positions.filter { p =>
      nextStep(world.cell(p), neighborhood(world).countLiveNeighbors(p)) == Cell.Live
    }.foldLeft(worldFactory(world.width, world.height))(_.populate(_))
  }
}
