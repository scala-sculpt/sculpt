import life.api.{Cell, World}

class WorldImpl(val width: Int, val height: Int, lives: Set[(Int, Int)]) extends World {
  def populate(position: (Int, Int)): World = new WorldImpl(width, height, lives + position)

  def cell(position: (Int, Int)): Cell = if (lives(position)) Cell.Live else Cell.Dead
}

object WorldImpl {
  def apply(width: Int, height: Int): WorldImpl = new WorldImpl(width, height, Set())
}