import life.api.{Cell, Neighborhood, World}

class NeighborhoodImpl(world: World) extends Neighborhood {

  def neighbors(position: (Int, Int)): Seq[(Int, Int)] = position match {
    case (x, y) =>
      for {
        xn <- (x - 1) to (x + 1)
        yn <- (y - 1) to (y + 1)
        if (xn, yn) != (x, y) &&
          xn >= 0 && xn < world.width &&
          yn >= 0 && yn < world.height
      } yield (xn, yn)
  }

  def countLiveNeighbors(position: (Int, Int)): Int = neighbors(position).count(world.cell(_) == Cell.Live)
}
