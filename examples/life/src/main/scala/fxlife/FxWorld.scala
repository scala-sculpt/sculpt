package fxlife

import life.api.World
import scalafx.beans.property.ObjectProperty
import scalafx.geometry.Pos
import scalafx.scene.layout.GridPane

class FxWorld(worldProp: ObjectProperty[World]) extends GridPane {
  import scalafx.Includes._

  alignment = Pos.Center
  vgap = 2
  hgap = 2

  refresh(worldProp.value)
  worldProp.onChange((_, _, world) => refresh(world))

  private def refresh(world: World){
    children = Seq()
    for {
      x <- 0 until world.width
      y <- 0 until world.height
    } {
      val fxCell = new FxCell(world.cell(x, y))
      fxCell.onMouseClicked = () => {
        worldProp.value = world.populate(x, y)
      }
      add(fxCell, x, y)
    }
  }
}
