package fxlife

import scalafx.Includes._
import scalafx.animation.Timeline
import scalafx.scene.layout.VBox
import scalafx.scene.paint.Color
import scalafx.scene.shape.{Polygon, Rectangle, Shape}

class ToolBar(timeline: Timeline) extends VBox {
  spacing = 16

  private val playButton = Polygon(0d, 0d, 31.18d, 18d, 0d, 36d)
  private val stopButton = new Rectangle {
    height = 28
    width = 28
  }

  private def setFill(shape: Shape): Unit = {
    shape.fill <== when(shape.hover) choose {
      Color.rgb(60, 200, 60)
    } otherwise {
      Color.rgb(50, 150, 50)
    }
  }

  setFill(playButton)
  setFill(stopButton)

  playButton.onMouseClicked = () => timeline.play()
  stopButton.onMouseClicked = () => timeline.stop()

  children = Seq(playButton, stopButton)

}
