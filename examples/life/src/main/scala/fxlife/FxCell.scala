package fxlife

import life.api.Cell
import scalafx.Includes.when
import scalafx.scene.Group
import scalafx.scene.paint.Color
import scalafx.scene.shape.{Circle, Rectangle}

class FxCell(cell: Cell) extends Group() {

  private val rectangle = new Rectangle {
    height = 24
    width = 24
    fill = Color.rgb(42, 40, 48)
  }

  private val circle = new Circle {
    centerX = 12
    centerY = 12
    radius = 10
  }

  circle.fill <== (cell match {
    case Cell.Live =>
      when(circle.hover) choose Color.rgb(100, 30, 30) otherwise Color.rgb(150, 20, 20)
    case Cell.Dead =>
      when(hover) choose Color.rgb(64, 60, 70) otherwise Color.gray(0, 0)
  })

  children = Seq(rectangle, circle)
}
