package fxlife

import life.GameOfLife
import life.api.World
import scalafx.Includes._
import scalafx.animation.{KeyFrame, Timeline}
import scalafx.application.JFXApp
import scalafx.application.JFXApp.PrimaryStage
import scalafx.beans.property.ObjectProperty
import scalafx.scene.Scene
import scalafx.scene.layout.{HBox, VBox}
import scalafx.scene.paint.Color._
import scalafx.scene.paint.{Color, LinearGradient, Stops}
import scalafx.scene.text.Text
import sculpt.runtime.RuntimeSolver
import sculpt.runtime.scalatest.ScalaTestSupport

import scala.language.postfixOps

object FxLife extends RuntimeSolver(GameOfLife.source) with JFXApp with ScalaTestSupport {
  stage = resolve(GameOfLife.potential).map(init).getOrElse(error)

  private def init(game: GameOfLife): PrimaryStage = {
    val worldProp: ObjectProperty[World] = ObjectProperty(game.world(16, 16))
    val fxWorld = new FxWorld(worldProp)
    val nextStep = () => {
      worldProp.value = game.life.nextStep(worldProp.value)
    }
    val timeline = new Timeline {
      cycleCount = Timeline.Indefinite
      keyFrames = Seq(KeyFrame(0.2 s, onFinished = nextStep))
    }
    val toolBar = new ToolBar(timeline)
    new PrimaryStage {
      title = "ScalaFX Axiom for Life"
      scene = new Scene {
        fill = Color.rgb(32, 30, 34)
        content = new VBox {
          spacing = 8
          children = Seq(
            titleText,
            new HBox {
              spacing = 8
              children = Seq(fxWorld, toolBar)
            }
          )
        }
      }
    }
  }

  private def error: PrimaryStage = {
    new PrimaryStage {
      title = "ScalaFX Axiom for Life"
      scene = new Scene {
        fill = Color.rgb(32, 30, 34)
        content = titleText
      }
    }
  }

  private def titleText: Text = {
    new Text {
      text = "Game Of Life"
      style = "-fx-font-size: 48pt"
      fill <== {
        when(hover) choose {
          new LinearGradient(endX = 0, stops = Stops(Cyan, GreenYellow))
        } otherwise {
          new LinearGradient(endX = 0, stops = Stops(Cyan, DodgerBlue))
        }
      }
    }
  }
}
