package life.api

import axiom.{AxiomDrivenPropertyChecks, Given}
import org.scalatest.{FreeSpec, Matchers}

trait Life {
  def nextStep(cell: Cell, neighborCount: Int): Cell
  def nextStep(world: World): World
}

class LifeSpec(worldFactory: World.Factory, neighborhood: Neighborhood.Factory)(life: Life)
  extends FreeSpec with AxiomDrivenPropertyChecks with Matchers with GivenWorld {

  "a live cell with fewer than two live cells dies" in axiom {
    for {
      cell <- Given.const(Cell.Live)
      neighborsCount <- Given.oneOf(0, 1)
    } yield life.nextStep(cell, neighborsCount) shouldBe Cell.Dead
  }

  "a dead cell with exactly three neighbors becomes alive" in axiom {
    for {
      cell <- Given.const(Cell.Dead)
      neighborsCount <- Given.const(3)
    } yield life.nextStep(cell, neighborsCount) shouldBe Cell.Live
  }

  "a live cell with two or three neighbors stays alive" in axiom {
    for {
      cell <- Given.const(Cell.Live)
      neighborsCount <- Given.oneOf(2, 3)
    } yield life.nextStep(cell, neighborsCount) shouldBe Cell.Live
  }

  "a live cell with more than three neighbors dies" in axiom {
    for {
      cell <- Given.const(Cell.Live)
      neighborsCount <- Given.oneOf(4 to 8)
    } yield life.nextStep(cell, neighborsCount) shouldBe Cell.Dead
  }

  "next step world is the world of next step cells" in axiom {
    for {
      world <- worldFactory.anyWorld
      pos <- world.anyPosition
      neighborCount = neighborhood(world).countLiveNeighbors(pos)
    } yield life.nextStep(world).cell(pos) shouldBe life.nextStep(world.cell(pos), neighborCount)
  }
}
