package life.api

import axiom.{AxiomDrivenPropertyChecks, Given}
import org.scalatest.{FreeSpec, Matchers}

trait World {
  val width: Int
  val height: Int
  def populate(position: (Int, Int)): World
  def cell(position: (Int, Int)): Cell
}

object World {
  type Factory = (Int, Int) => World
}

class WorldSpec(factory: World.Factory) extends FreeSpec with AxiomDrivenPropertyChecks with Matchers with GivenWorld {

  "new world should be of size width x height" in axiom {
    for {
      (width, height) <- Given.sized2
      world = factory(width, height)
    } yield {
      world.width shouldBe width
      world.height shouldBe height
    }
  }

  "any cell should be dead in a new world" in axiom {
    for {
      world <- factory.newWorld
      pos <- world.anyPosition
    } yield world.cell(pos) shouldBe Cell.Dead
  }

  "when populating a cell, it should live" in axiom {
    for {
      world <- factory.anyWorld
      pos <- world.anyPosition
    } yield world.populate(pos).cell(pos) shouldBe Cell.Live
  }

  "when populating a cell, no other cell should change" in axiom {
    for {
      world <- factory.anyWorld
      pos1 <- world.anyPosition
      pos2 <- world.anyPosition if pos1 != pos2
    } yield world.populate(pos1).cell(pos2) shouldBe world.cell(pos2)
  }
}

trait GivenWorld {
  implicit class WorldFactoryExtension(factory: World.Factory) {
    val newWorld: Given[World] = {
      for {
        (width, height) <- Given.sized2 if width > 0 && height > 0
      } yield factory(width, height)
    }

    val anyWorld: Given[World] = {
      for {
        world <- newWorld
        n <- Given.oneOf(0 until world.width * world.height)
        liveCells <- Given.pick(n, world.positions)
      } yield liveCells.foldLeft(world)(_.populate(_))
    }
  }

  implicit class AnyPositionExtension(world: World) {
    val positions: Seq[(Int, Int)] = {
      for {
        x <- 0 until world.width
        y <- 0 until world.height
      } yield (x, y)
    }

    val anyPosition: Given[(Int, Int)] = Given.oneOf(positions)
  }
}