package life.api

sealed abstract class Cell

object Cell {
  case object Live extends Cell
  case object Dead extends Cell
}

