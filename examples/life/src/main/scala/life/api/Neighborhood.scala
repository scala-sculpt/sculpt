package life.api

import axiom.{AxiomDrivenPropertyChecks, Given}
import org.scalatest.{FreeSpec, Matchers}

trait Neighborhood {
  def neighbors(position: (Int, Int)): Seq[(Int, Int)]
  def countLiveNeighbors(position: (Int, Int)): Int
}

object Neighborhood {
  type Factory = World => Neighborhood
}

class NeighborhoodSpec(worldFactory: World.Factory)(neighborhood: Neighborhood.Factory)
  extends FreeSpec with AxiomDrivenPropertyChecks with Matchers with GivenWorld {

  "a neighbor is inside the world" in axiom {
    for {
      world <- worldFactory.anyWorld
      pos <- world.anyPosition
      (neighborX, neighborY) <- Given.oneOf(neighborhood(world).neighbors(pos))
    } yield {
      neighborX should (be >= 0 and be < world.width)
      neighborY should (be >= 0 and be < world.height)
    }
  }

  "a neighbor of the cell is next to the cell" in axiom {
    for {
      world <- worldFactory.anyWorld
      (x, y) <- world.anyPosition
      (neighborX, neighborY) <- Given.oneOf(neighborhood(world).neighbors(x, y))
    } yield {
      math.abs(neighborX - x) should be <= 1
      math.abs(neighborY - y) should be <= 1
    }
  }

  "a neighbor of a cell is not the cell" in axiom {
    for {
      world <- worldFactory.anyWorld
      pos <- world.anyPosition
      neighbor <- Given.oneOf(neighborhood(world).neighbors(pos))
    } yield neighbor should not be pos
  }

  "the number of live neighbors is between 0 and 8 included" in axiom {
    for {
      world <- worldFactory.anyWorld
      pos <- world.anyPosition
    } yield neighborhood(world).countLiveNeighbors(pos) should (be >= 0 and be <= 8)
  }

  "the number of live neighbors increases by one when populating a dead neighbor" in axiom {
    for {
      world <- worldFactory.anyWorld
      pos <- world.anyPosition
      neighbor <- Given.oneOf(neighborhood(world).neighbors(pos)) if world.cell(neighbor) == Cell.Dead
    } yield neighborhood(world.populate(neighbor)).countLiveNeighbors(pos) shouldBe neighborhood(world).countLiveNeighbors(pos) + 1
  }
}
