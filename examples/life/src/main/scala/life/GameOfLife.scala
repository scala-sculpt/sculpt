package life

import life.api._
import sculpt.Potential
import sculpt.scalatest.{ScalaTestSculpting, Spec}
import sculpt.source.{Resource, Source}

case class GameOfLife(world: World.Factory, life: Life)

object GameOfLife extends ScalaTestSculpting {
  val source: Source = Resource("/source")

  val potential: Potential[GameOfLife, Spec] = for {
    world <- any[World.Factory].satisfying(w => new WorldSpec(w))
    neighborhood <- any[Neighborhood.Factory].satisfying(new NeighborhoodSpec(world)(_))
    life <- any[Life].satisfying(new LifeSpec(world, neighborhood)(_))
  } yield GameOfLife(world, life)
}