package life

import sculpt.junit.JUnitSuite
import sculpt.junit.scalatest.ScalaTestSupport

class GameOfLifeJUnitSuite extends JUnitSuite(GameOfLife.source) with ScalaTestSupport {
  test(GameOfLife.potential)
}
