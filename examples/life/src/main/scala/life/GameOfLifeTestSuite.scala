package life

import sculpt.test.SculptSuite
import sculpt.test.scalatest.ScalaTestSupport

class GameOfLifeTestSuite extends SculptSuite(GameOfLife.source) with ScalaTestSupport {
  test(GameOfLife.potential)
}
