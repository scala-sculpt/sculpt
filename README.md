# Sculpt

Sculpt is an experimental library for test-safe programming.

## Test-safe programming

Sculpt emerged from the idea that tests are sometimes more expressive than implementation at describing the software. Tests are different from implementation in many aspects :
  - tests are not exhaustive
  - tests are strongly decoupled
  - tests are focused on a particular aspect: functionality, performance, security

Traditionally, the tests are bound to a particular implementation. Yet the tests could be used to validate or invalidate many implementations. In test-safe programming, the dependency between implementations and tests is inverted.

````scala
// a domain interface
trait PetStore { ... }

// a specification of PetStore, it takes a PetStore at construction
class PetStoreSpec(petStore: PetStore) { ... }

object Main extends App with Sculpting {
  val petStore: Potential[PetStore] = any[PetStore].satisfying(new PetStoreSpec(_))
}
````

In this example `PetStoreSpec` does not depend upon a particular `PetStore`. In the main program, we explicitly say that `petStore` must satisfy a particular specification (or many). That's why the program is said to be test-safe.

In test-safe programing, we are forced to write the test before the implementation to ensure the validity of the software.  Not only the specification must validate a good implementation but it must also invalidate bad implementations. Test-safe programming is somehow TDD by design.

The specification is not frozen, it matures along with the understanding of the customer requirements. Test-safe programing values the specification more than the implementation and sees programing as a designing process.

## Terminology

### `Potential[A, Spec[_]]`

A potential implementation of type `A` satisfying some specifications of type `Spec[_]`. This implementation exists in the elevated world of potentialities.

### `Solver[Res[_], Spec[_]]`

A solver resolves a potential of `A` in a result of `A`. A solver is an interpreter of potentials.

Three solvers are provided in this repository:
- `RuntimeSolver[Spec[_]]` resolves to `Either[RuntimeError, A]`
- `JUnitSolver[Spec[_]]` resolves to an `ExecutionTree[A]` for resolution in a `JUnitRunner`. Caution, Junit does not support Sculpt so well.
- `TestSolver[Spec[_]]` resolves to an `ExecutionTree[A]` for resolution in the Sculpt test framework. The Sculpt Intellij Idea plugin supports the Sculpt test framework.

### `Source`

Every provided solver relies on a `Source`.

The role of a `Source` is to creates the implementations of a given type regardless of the specifications. The only provided implementation, called `Resource`, compiles the files in a resource folder.

## Philosophy

Sculpt focuses on the relationship between the specification and the implementation. It aims at making this relationship explicit. Sculpt evolves toward flexibility and expressiveness.

The purpose of the provided solvers is to prove the concept of test-safe programming. Those solvers are not the heart of Sculpt.

Sculpt is build for modularity. It is compatible with any test paradigm and extensible with any custom solver.

## Getting Started

TODO

## Structure

- `sculpt` > core
- `sculpt-junit` > junit solver and runner
- `sculpt-test` > custom solver and test framework
- `examples` 
  - `life` > a test-safe program of the Game of Life
- `intellij-plugin` > plugin for Intellij Idea 
- `axiom` > for comprehension style for property-based testing (third party project used in examples/life)

## Intellij Idea Plugin

The intellij idea plugin adds support to the Sculpt test framework

