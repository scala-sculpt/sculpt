package sculpt.junit

import org.junit.runner.Description

trait Descriptions {
  def suiteDescription(suiteName: String, children: Seq[Description]): Description = {
    val description = Description.createSuiteDescription(escape(suiteName))
    children.foreach(description.addChild)
    description
  }

  def suiteDescription(suiteName: String, suiteId: Int, children: Seq[Description]): Description = {
    val description = Description.createSuiteDescription(suiteName, suiteId)
    children.foreach(description.addChild)
    description
  }

  def testDescription(suiteName: String, suiteId: Int, testName: String): Description = {
    Description.createTestDescription(escape(suiteName), escape(testName), suiteName + testName + suiteId)
  }

  private def escape(str: String): String = {
    str.replaceAll("\\.", "'")
      .replaceAll("\\(", "[")
      .replaceAll("\\)", "]")
  }
}
