package sculpt.junit

import org.junit.runner.RunWith
import sculpt.source.Source

import scala.collection.mutable.ListBuffer
import scala.language.higherKinds

@RunWith(classOf[JUnitRunner])
abstract class JUnitSuite(source: Source) {
  private[junit] val executionTrees = ListBuffer.empty[ExecutionTree[Any]]

  def test[A, Spec[_]: Support](potential: sculpt.Potential[A, Spec]): Unit = {
    executionTrees ++= potential.resolveWith(new JUnitSolver[Spec](source))
  }
}
