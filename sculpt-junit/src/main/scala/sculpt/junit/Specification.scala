package sculpt.junit

import org.junit.runner.Description
import org.junit.runner.notification.RunNotifier

trait Specification {
  def description: Description
  def run(notifier: RunNotifier): Status
  def ignore(notifier: RunNotifier): Unit
}
