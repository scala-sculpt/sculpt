package sculpt.junit

import org.junit.runner.notification.RunNotifier
import sculpt.source.Impl

sealed trait ExecutionTree[+A] {
  def descriptions: Seq[Candidate.Description]
  def run(notifier: RunNotifier): Status
  def ignore(notifier: RunNotifier): Unit
}

object ExecutionTree {
  final case class Node[+A](candidate: Candidate, children: Seq[ExecutionTree[A]]) extends ExecutionTree[A] {
    override def descriptions: Seq[Candidate.Description] = {
      candidate.description +: children.flatMap(_.descriptions).map { case Candidate.Description(path, spec) =>
        Candidate.Description(candidate.description.path ++ path, spec)
      }
    }

    override def run(notifier: RunNotifier): Status = {
      candidate.run(notifier) match {
        case Status.Success => children.foldLeft(Status.failure) {
          case (Status.Success, child) => child.ignore(notifier); Status.Success
          case (Status.Failure, child) => child.run(notifier)
        }
        case Status.Failure => children.foreach(_.ignore(notifier)); Status.Failure
      }
    }

    override def ignore(notifier: RunNotifier): Unit = {
      candidate.ignore(notifier)
      children.foreach(_.ignore(notifier))
    }
  }

  final case class Leaf[A](candidate: Candidate, impl: Impl[A]) extends ExecutionTree[A] {
    override def descriptions: Seq[Candidate.Description] = Seq(candidate.description)
    override def run(notifier: RunNotifier): Status = candidate.run(notifier)
    override def ignore(notifier: RunNotifier): Unit = candidate.ignore(notifier)
  }
}
