package sculpt.junit.scalatest

import org.junit.runner.Description
import org.junit.runner.notification.{Failure, RunNotifier}
import org.scalatest.events._
import org.scalatest.{Args, Reporter, Suite}
import sculpt.junit.{Descriptions, Specification, Status}

class ScalaTestSpecification(main: Suite) extends Specification with Descriptions {
  private val specId = main.hashCode

  override def description: Description = suiteDescription(main)

  override def run(notifier: RunNotifier): Status = {
    val reporter = new ScalaTestReporter(notifier)
    if (main.run(None, Args(reporter)).succeeds()) Status.Success
    else Status.Failure
  }

  override def ignore(notifier: RunNotifier): Unit = ignoreSuite(notifier)(main)

  private def suiteDescription(suite: Suite): Description = {
    suiteDescription(
      suite.suiteName,
      suite.suiteName.hashCode + specId,
      suite.nestedSuites.map(suiteDescription) ++ suite.testNames.map(testDescription(suite.suiteName))
    )
  }

  private def testDescription(suiteName: String)(testName: String): Description = {
    testDescription(suiteName, suiteName.hashCode + specId, testName)
  }

  private def ignoreSuite(notifier: RunNotifier)(suite: Suite): Unit = {
    suite.testNames.map(testDescription(suite.suiteName)).foreach(notifier.fireTestIgnored)
    suite.nestedSuites.foreach(ignoreSuite(notifier))
  }

  private class ScalaTestReporter(notifier: RunNotifier) extends Reporter {
    override def apply(event: Event): Unit = event match {
      case TestStarting(_, suiteName, _, _, testName, _, _, _, _, _, _, _) =>
        notifier.fireTestStarted(testDescription(suiteName)(testName))

      case TestFailed(_, _, suiteName, _, _, testName, _, _, throwable, _, _, _, _, _, _, _) =>
        notifier.fireTestFailure(new Failure(testDescription(suiteName)(testName), throwable.orNull))

      case TestSucceeded(_, suiteName, _, _, testName, _, _, _, _, _, _, _, _, _) =>
        notifier.fireTestFinished(testDescription(suiteName)(testName))

      case TestIgnored(_, suiteName, _, _, testName, _, _, _, _, _, _) =>
        notifier.fireTestIgnored(testDescription(suiteName)(testName))

      case TestPending(_, suiteName, _, _, testName, _, _, _, _, _, _, _, _) =>
        notifier.fireTestIgnored(testDescription(suiteName)(testName))

      case SuiteAborted(_, _, suiteName, _, _, throwable, _, _, _, _, _, _, _) =>
        val throwableOrNull =
          throwable match {
            case Some(t) => t
            case None => null
          }
        val description = Description.createSuiteDescription(s"Specification $suiteName")
        notifier.fireTestFailure(new Failure(description, throwableOrNull))
        notifier.fireTestFinished(description)

      // case RunAborted(_, message, throwable, _, _, _, _, _, _, _) =>
      case _ =>
    }
  }
}
