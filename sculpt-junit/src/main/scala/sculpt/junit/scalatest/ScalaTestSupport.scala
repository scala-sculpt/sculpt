package sculpt.junit.scalatest

import sculpt.junit.{Specification, Support}
import sculpt.scalatest.Spec

trait ScalaTestSupport {
  implicit val support: Support[Spec] = new Support[Spec] {
    override def toJunit[A](spec: Spec[A])(a: A): Specification = new ScalaTestSpecification(spec(a))
  }
}
