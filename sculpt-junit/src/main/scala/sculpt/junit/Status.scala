package sculpt.junit

sealed trait Status {
  def &&(other: Status): Status
}

object Status {
  val failure: Status = Status.Failure

  final case object Success extends Status {
    override def &&(other: Status): Status = other
  }

  final case object Failure extends Status {
    override def &&(other: Status): Status = Failure
  }
}
