package sculpt.junit

import org.junit.runner
import org.junit.runner.notification.RunNotifier

case class Candidate(ref: Candidate.Ref, specifications: Seq[Specification]) {
  def description: Candidate.Description = {
    Candidate.Description(Seq(ref), specifications.map(_.description))
  }

  def run(notifier: RunNotifier): Status = {
    specifications.map(_.run(notifier)).fold(Status.Success)(_ && _)
  }

  def ignore(notifier: RunNotifier): Unit = {
    specifications.foreach(_.ignore(notifier))
  }
}

object Candidate {
  case class Ref(potential: String, candidate: String)

  case class Description(path: Seq[Candidate.Ref], specifications: Seq[runner.Description]) extends Descriptions {
    def toJunit: runner.Description = suiteDescription(name, specifications)
    def level: Seq[String] = path.map(_.potential)
    private def name: String = path.map(_.candidate).mkString(" -> ")
  }
}