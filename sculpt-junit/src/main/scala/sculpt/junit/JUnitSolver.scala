package sculpt.junit

import sculpt.Solver
import sculpt.junit.ExecutionTree.{Leaf, Node}
import sculpt.junit.JUnitSolver.Execution
import sculpt.source.{Impl, Source}

import scala.language.higherKinds
import scala.reflect.runtime.universe.{TypeTag, WeakTypeTag, typeOf, weakTypeOf}

class JUnitSolver[Spec[_]: Support](source: Source) extends Solver[Execution, Spec] {
  override def any[A: TypeTag]: Execution[A] = {
    val potential = typeOf[A].toString
    source.find[A].map { impl =>
      Leaf(Candidate(Candidate.Ref(potential, impl.name), Seq()), impl)
    }
  }

  override def weakAny[A[_], T](implicit tt: WeakTypeTag[A[T]]): Execution[A[T]] = {
    val potential = weakTypeOf[A[T]].toString
    source.weakFind[A, T].map { impl =>
      Leaf(Candidate(Candidate.Ref(potential, impl.name), Seq()), impl)
    }
  }

  override def map[A, B](execution: Execution[A])(f: A => B): Execution[B] = {
    execution.map {
      case Leaf(candidate, impl) => Leaf(candidate, impl.map(f))
      case Node(candidate, children) => Node(candidate, map(children)(f))
    }
  }

  override def flatMap[A, B](execution: Execution[A])(f: Impl[A] => Execution[B]): Execution[B] = {
    execution.map {
      case Leaf(candidate, impl) => Node(candidate, f(impl))
      case Node(candidate, children) => Node(candidate, flatMap(children)(f))
    }
  }

  override def satisfying[A](execution: Execution[A])(spec: Spec[A]): Execution[A] = {
    execution.map {
      case Leaf(candidate, impl) =>
        val support = implicitly[Support[Spec]]
        Leaf(candidate.copy(specifications = candidate.specifications :+ support.toJunit(spec)(impl.value)), impl)
      case Node(candidate, children) => Node(candidate, satisfying(children)(spec))
    }
  }

  override def given[A](impl: Impl[A]): Solver[Execution, Spec] = {
    new JUnitSolver(source.given(impl))
  }
}

object JUnitSolver {
  type Execution[A] = Seq[ExecutionTree[A]]
}
