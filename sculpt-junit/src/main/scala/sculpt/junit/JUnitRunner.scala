package sculpt.junit

import org.junit.runner.notification.{Failure, RunNotifier}
import org.junit.runner.{Description, Runner}

import scala.util.control.NonFatal

private class JUnitRunner(suiteClass: java.lang.Class[_ <: JUnitSuite]) extends Runner with Descriptions {
  private val suite = suiteClass.getConstructor().newInstance()

  override def getDescription: Description = {
    val potentials = suite.executionTrees.flatMap(_.descriptions).groupBy(_.level).map {
      case (level, candidates) => levelDescriptions(level, candidates)
    }.toSeq
    suiteDescription(suiteClass.getSimpleName, potentials)
  }

  override def run(notifier: RunNotifier): Unit = {
    try {
      suite.executionTrees.foreach(_.run(notifier))
    } catch {
      case NonFatal(cause) => notifier.fireTestFailure(new Failure(getDescription, cause))
    }
  }

  private def levelDescriptions(level: Seq[String], candidates: Seq[Candidate.Description]): Description = {
    suiteDescription(s"Potential ${level.last}", candidates.map(_.toJunit))
  }
}
