package sculpt.junit

import scala.language.higherKinds

trait Support[Spec[_]] {
  def toJunit[A](spec: Spec[A])(a: A): Specification
}
