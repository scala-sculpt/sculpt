package sculpt

import org.scalatest.{FreeSpec, Matchers}
import sculpt.reflection.{Composition, Decomposer}

import scala.language.higherKinds
import scala.reflect.runtime.universe

trait Yo[A] {
  def yo: A
}

trait Yi[A] {
  def yi: A
}

object Input {


  object Yo1 extends Yo[String] {
    def yo: String = "yo"
  }

  class Yi1[A](yo: Yo[A]) extends Yi[A] {
    def yi: A = yo.yo
  }
}

class Tests extends FreeSpec with Matchers {
  val composition: Composition = Decomposer.decompose(Input)

  import scala.reflect.runtime.universe.WeakTypeTag

  case class Foo[A[_], B[_], U, V]()(implicit tta: WeakTypeTag[A[U]], ttn: WeakTypeTag[B[V]]) {
    def foo: WeakTypeTag[A[U] => B[V]] = implicitly[WeakTypeTag[({type HF[X] = A[U] => B[X]})#HF[V]]]
  }

  "test yo" in {
    composition.weakFind[Yo, String].head.value.yo shouldBe "yo"
  }

  "test yi given yo" in {
    composition.weakFind[({type HF[T] = Yo[T] => Yi[T]})#HF, String].head.value(Input.Yo1).yi shouldBe "yo"
  }

  "test foo" in {
    println(Foo[Option, List, Int, String].foo.toString())
  }
}
