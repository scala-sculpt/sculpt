package sculpt.source

import sculpt.reflection.{Composition, ReplCompiler}

import scala.reflect.io.Directory

object Resource {
  def apply(name: String): Source = {
    val resource = getClass.getResource(name)
    if (resource == null) {
      new Composition(Map())
    } else {
      val folder = Directory(getClass.getResource(name).getPath)
      folder.deepFiles.flatMap(ReplCompiler.compile).fold(new Composition(Map()))(_.merge(_))
    }
  }
}
