package sculpt.source

import scala.language.higherKinds
import scala.reflect.runtime.universe.{TypeTag, WeakTypeTag}

trait Source {
  def find[A: TypeTag]: Seq[Impl[A]]
  def weakFind[A[_], T](implicit tt: WeakTypeTag[A[T]]): Seq[Impl[A[T]]]
  final def given[A](impl: Impl[A]): Source = impl.enrich(this)
}
