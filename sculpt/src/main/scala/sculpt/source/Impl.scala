package sculpt.source

import scala.language.{higherKinds, reflectiveCalls}
import scala.reflect.runtime.universe.{TypeTag, WeakTypeTag}

sealed trait Impl[+A] {
  def name: String
  def value: A
  def enrich(source: Source): Source

  def map[B](f: A => B): Impl[B] = DeadImpl(name, f(value))
  def safeMap[B: TypeTag](f: A => B): Impl[B] = SafeImpl(name, f(value))
  def weakMap[B[_], T](f: A => B[T])(implicit tt: WeakTypeTag[B[T]]): Impl[B[T]] = WeakImpl(name, f(value))
}

case class SafeImpl[A](name: String, value: A)(implicit typeTag: TypeTag[A]) extends Impl[A] {
  def enrich(source: Source): Source = new Source {
    def find[B: TypeTag]: Seq[Impl[B]] = {
      source.find[B] ++ source.find[A => B].map(impl => impl.map(f => f(value)))
    }

    def weakFind[B[_], T](implicit tt: WeakTypeTag[B[T]]): Seq[Impl[B[T]]] = {
      source.weakFind[B, T] ++ source.weakFind[({type HF[U] = A => B[U]})#HF, T].map(impl => impl.weakMap(f => f(value)))
    }
  }
}

case class WeakImpl[A[_], T](name: String, value: A[T])(implicit weakTypeTag: WeakTypeTag[A[T]]) extends Impl[A[T]] {
  def enrich(source: Source): Source = new Source {
    def find[B: TypeTag]: Seq[Impl[B]] = {
      source.find[B] ++  source.weakFind[({type HF[U] = A[U] => B})#HF, T].map(impl => impl.map(f => f(value)))
    }

    def weakFind[B[_], U](implicit tt: WeakTypeTag[B[U]]): Seq[Impl[B[U]]] = {
      source.weakFind[B, U] ++ source.weakFind[({type HF[V] = A[T] => B[V]})#HF, U].map(impl => impl.weakMap(f => f(value)))
    }
  }
}

case class DeadImpl[A](name: String, value: A) extends Impl[A] {
  def enrich(source: Source): Source = source
}