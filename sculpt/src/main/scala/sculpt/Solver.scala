package sculpt

import sculpt.source.{Impl, Source}

import scala.language.higherKinds
import scala.reflect.runtime.universe.{TypeTag, WeakTypeTag}

trait Solver[Res[_], Spec[_]] {
  def resolve[A](potential: Potential[A, Spec]): Res[A] = potential.resolveWith(this)

  def any[A: TypeTag]: Res[A]
  def weakAny[A[_], T](implicit tt: WeakTypeTag[A[T]]): Res[A[T]]

  def map[A, B](res: Res[A])(f: A => B): Res[B]
  def flatMap[A, B](res: Res[A])(f: Impl[A] => Res[B]): Res[B]

  def satisfying[A](res: Res[A])(spec: Spec[A]): Res[A]
  def given[A](impl: Impl[A]): Solver[Res, Spec]
}
