package sculpt.runtime.scalatest

import org.scalatest.events.Event
import org.scalatest.{Args, Reporter}
import sculpt.runtime.Support
import sculpt.scalatest.Spec

trait ScalaTestSupport {
  private val NoReporter = new Reporter {
    override def apply(event: Event): Unit = ()
  }

  implicit val support: Support[Spec] = new Support[Spec] {
    override def validate[A](spec: Spec[A])(a: A): Boolean = {
      spec(a).run(None, Args(NoReporter)).succeeds()
    }
  }
}
