package sculpt.runtime.internal

import sculpt.Solver
import sculpt.runtime.RuntimeError.{NoImplementation, NoValidImplementation}
import sculpt.runtime.internal.RuntimeSolverImpl.Result
import sculpt.runtime.{RuntimeError, Support}
import sculpt.source.{Impl, Source}

import scala.language.higherKinds
import scala.reflect.runtime.universe.{TypeTag, WeakTypeTag}

case class RuntimeSolverImpl[Spec[_]: Support](source: Source) extends Solver[Result, Spec] {
  override def any[A: TypeTag]: Result[A] = {
    source.find[A].toList match {
      case head::tail => Right(::(head, tail))
      case Nil => Left(NoImplementation)
    }
  }

  override def weakAny[A[_], T](implicit tt: WeakTypeTag[A[T]]): Result[A[T]] = {
    source.weakFind[A, T].toList match {
      case head::tail => Right(::(head, tail))
      case Nil => Left(NoImplementation)
    }
  }

  override def map[A, B](res: Result[A])(f: A => B): Result[B] = {
    res.map(cons => ::(cons.head.map(f), cons.tail.map(impl => impl.map(f))))
  }

  override def flatMap[A, B](res: Result[A])(f: Impl[A] => Result[B]): Result[B] = {
    for {
      as <- res
      bs <- RuntimeSolverImpl.traverse(as)(f)
    } yield bs
  }

  override def satisfying[A](res: Result[A])(spec: Spec[A]): Result[A] = {
    val validate = implicitly[Support[Spec]].validate(spec)(_)
    res.flatMap { as =>
      as.filter(impl => validate(impl.value)) match {
        case head :: tail => Right(::(head, tail))
        case Nil => Left(NoValidImplementation)
      }
    }
  }

  override def given[A](impl: Impl[A]): Solver[Result, Spec] = new RuntimeSolverImpl[Spec](source.given(impl))
}

object RuntimeSolverImpl {
  type Result[A] = Either[RuntimeError, ::[Impl[A]]]

  private def traverse[A, B](as: List[A])(f: A => Result[B]): Result[B] = {
    as.map(f).fold[Result[B]](Left(NoImplementation)){
      case (Right(bs1), Right(bs2)) => Right(::(bs1.head, bs1.tail ++ bs2))
      case (Right(bs), Left(_)) => Right(bs)
      case (Left(_), Right(bs)) => Right(bs)
      case (Left(err1), Left(err2)) => Left(RuntimeError.combine(err1, err2))
    }
  }
}
