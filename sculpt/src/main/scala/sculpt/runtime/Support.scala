package sculpt.runtime

trait Support[Spec[_]] {
  def validate[A](spec: Spec[A])(a: A): Boolean
}
