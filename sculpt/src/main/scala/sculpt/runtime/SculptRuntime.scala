package sculpt.runtime

import sculpt.Potential
import sculpt.source.Source

abstract class SculptRuntime(source: Source) {
  private val solver = new RuntimeSolver(source)

  def run[A, Spec[_]: Support](potential: Potential[A, Spec])(f: A => Unit): Unit = {
    solver.resolve(potential) match {
      case Left(error) => throw new Exception(error.toString)
      case Right(a) => f(a)
    }
  }
}
