package sculpt.runtime

import sculpt.Potential
import sculpt.runtime.internal.RuntimeSolverImpl
import sculpt.source.Source

import scala.language.higherKinds

class RuntimeSolver(source: Source) {
  def resolve[A, Spec[_]: Support](potential: Potential[A, Spec]): Either[RuntimeError, A] = {
    RuntimeSolverImpl(source).resolve(potential).map(_.head.value)
  }
}

