package sculpt.runtime

sealed trait RuntimeError

object RuntimeError {
  case object NoImplementation extends RuntimeError
  case object NoValidImplementation extends RuntimeError

  def combine(err1: RuntimeError, err2: RuntimeError): RuntimeError = {
    (err1, err2) match {
      case (NoValidImplementation, _) => NoValidImplementation
      case (_, NoValidImplementation) => NoValidImplementation
      case _ => err1
    }
  }
}
