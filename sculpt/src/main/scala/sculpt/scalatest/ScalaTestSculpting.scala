package sculpt.scalatest

import sculpt.Potential

import scala.language.higherKinds
import scala.reflect.runtime.universe.{TypeTag, WeakTypeTag}

trait ScalaTestSculpting {
  def any[A: TypeTag]: Potential[A, Spec] = Potential.Any[A, Spec]()
  def any[A[_], T](implicit tt: WeakTypeTag[A[T]]): Potential[A[T], Spec] = Potential.WeakAny[A, Spec, T]()
}
