package sculpt

import org.scalatest.Suite

package object scalatest {
  type Spec[A] = A => Suite
}
