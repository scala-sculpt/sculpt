package sculpt

import scala.language.higherKinds
import scala.reflect.runtime.universe.{TypeTag, WeakTypeTag}

trait Potential[A, Spec[_]] {
  def map[B](f: A => B): Potential[B, Spec] = Potential.Map(this, f)
  def flatMap[B](f: A => Potential[B, Spec]): Potential[B, Spec] = Potential.FlatMap(this, f)
  def satisfying(spec: Spec[A]): Potential[A, Spec] = Potential.Satisfying(this, spec)

  private[sculpt] def resolveWith[Res[_]](solver: Solver[Res, Spec]): Res[A]
}

object Potential {
  final case class Any[A : TypeTag, Spec[_]]() extends Potential[A, Spec] {
    def resolveWith[Res[_]](solver: Solver[Res, Spec]): Res[A] = solver.any[A]
  }

  final case class WeakAny[A[_], Spec[_], T]()(implicit tt: WeakTypeTag[A[T]]) extends Potential[A[T], Spec] {
    def resolveWith[Res[_]](solver: Solver[Res, Spec]): Res[A[T]] = solver.weakAny[A, T]
  }

  final case class Map[A, B, Spec[_]](potential: Potential[A, Spec], f: A => B) extends Potential[B, Spec] {
    def resolveWith[Res[_]](solver: Solver[Res, Spec]): Res[B] = solver.map(potential.resolveWith(solver))(f)
  }

  final case class FlatMap[A, B, Spec[_]](potential: Potential[A, Spec], f: A => Potential[B, Spec]) extends Potential[B, Spec] {
    def resolveWith[Res[_]](solver: Solver[Res, Spec]): Res[B] = {
      solver.flatMap(potential.resolveWith(solver)) { impl =>
          f(impl.value).resolveWith(solver.given(impl))
      }
    }
  }

  final case class Satisfying[A, Spec[_]](potential: Potential[A, Spec], spec: Spec[A]) extends Potential[A, Spec] {
    def resolveWith[Res[_]](solver: Solver[Res, Spec]): Res[A] = solver.satisfying(potential.resolveWith(solver))(spec)
  }
}
