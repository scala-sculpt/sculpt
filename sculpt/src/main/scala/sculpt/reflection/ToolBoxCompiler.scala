package sculpt.reflection

import java.lang.reflect.InvocationTargetException

import scala.reflect.io.File
import scala.util.control.NonFatal

object ToolBoxCompiler extends Compiler {
  import scala.reflect.runtime.universe._
  import scala.tools.reflect.ToolBox

  private val mirror = runtimeMirror(getClass.getClassLoader)
  private val toolBox = mirror.mkToolBox()

  override def compile(file: File): Option[Composition] = {

    try {
      val content = file.lines().map(l => s"\t$l").mkString("\n")
      val wrappedContent = s"""
         |import specification.reflective.Decomposer
         |
         |object Wrapper {
         |$content
         |}
         |
         |Decomposer.decompose(Wrapper)
      """.stripMargin
      val tree = toolBox.parse(wrappedContent)
      val composition = toolBox.eval(tree).asInstanceOf[Composition]
      Some(composition)
    } catch {
      case e: InvocationTargetException => println(e.getCause); None
      case NonFatal(e) => println(e); None
    }
  }
}
