package sculpt.reflection

import scala.reflect.io.File
import scala.tools.nsc.Settings
import scala.tools.nsc.interpreter.{IMain, Results}

object ReplCompiler extends Compiler {
  private val settings = new Settings
  settings.usejavacp.value = true
  private val interpreter = new IMain(settings)

  override def compile(file: File): Option[Composition] = {
    val content = file.lines().map(l => s"\t$l").mkString("\n")
    val wrappedContent = s"""
      |import sculpt.reflection.Decomposer
      |object $$spec {
      |$content
      |}
      |val composition = Decomposer.decompose($$spec)
      """.stripMargin
    interpreter.interpret(wrappedContent) match {
      case Results.Success => interpreter.valueOfTerm("composition").map(_.asInstanceOf[Composition])
      case _ => None
    }
  }
}
