package sculpt.reflection

import scala.reflect.io.File

trait Compiler {
  def compile(file: File): Option[Composition]
}
