package sculpt.reflection

import scala.reflect.ClassTag
import scala.util.control.NonFatal

object Decomposer {
  import scala.reflect.runtime.universe._

  def decompose[T: ClassTag : TypeTag](wrapper: T): Composition = {

    val mirror = runtimeMirror(wrapper.getClass.getClassLoader)
    val wrapperType = typeOf[T]

    val wrapperMembers = wrapperType.members
    val wrapperModules = wrapperMembers.filter(_.isModule).map(_.asModule)
    val wrapperClasses = wrapperMembers.filter(_.isClass).map(_.asClass)

    val modulesComposition = wrapperModules.map(decomposeModule(None, mirror, _)).fold(new Composition(Map()))(_.merge(_))
    wrapperClasses.map(decomposeClass(None, mirror, _)).fold(modulesComposition)(_.merge(_))
  }

  def decomposeModule(sourceName: Option[String], mirror: Mirror, moduleSymbol: ModuleSymbol): Composition = {
    val moduleName = (sourceName.toSeq :+ moduleSymbol.name).mkString(".")
    val moduleMirror = mirror.reflectModule(moduleSymbol)
    val instanceMirror = mirror.reflect(moduleMirror.instance)

    val members = instanceMirror.symbol.info.members

    val methods = members.filter(m => m.isMethod && !m.isConstructor && !m.isImplementationArtifact).map(_.asMethod)
    val subModules = members.filter(_.isModule).map(_.asModule)

    val moduleComposition = new Composition(Map(moduleSymbol.info -> Seq((moduleName, moduleMirror.instance))))
    val methodCompositions = methods.map(methodComposition(moduleName, instanceMirror, _))
    val subModuleCompositions = subModules.map(decomposeModule(Some(moduleName), mirror, _))

    (methodCompositions ++ subModuleCompositions).fold(moduleComposition)(_.merge(_))
  }

  def decomposeClass(sourceName: Option[String], mirror: Mirror, classSymbol: ClassSymbol): Composition = {
    val className = (sourceName.toSeq :+ classSymbol.name).mkString(".")
    val classMirror = mirror.reflectClass(classSymbol)
    val constructorSymbol = classSymbol.primaryConstructor.asMethod
    val args = constructorSymbol.paramLists.flatten.map(_.asTerm).map(_.typeSignature)
    val result = constructorSymbol.returnType
    val constructorMirror = classMirror.reflectConstructor(constructorSymbol)

    args.size match {
      case 0 => methodComposition(className, constructorMirror, result)
      case 1 => methodComposition(className, constructorMirror, args.head, result)
      case 2 => methodComposition(className, constructorMirror, args.head, args(1), result)
      case 3 => methodComposition(className, constructorMirror, args.head, args(1), args(2), result)
      case i@ _ => println(s"not implemented $i"); new Composition(Map())
    }
  }

  def methodComposition(sourceName: String, sourceInstance: InstanceMirror, methodSymbol: MethodSymbol): Composition = {
    val methodName = s"$sourceName.${methodSymbol.name}"
    val args = methodSymbol.paramLists.flatten.map(_.asTerm).map(_.typeSignature)
    val result = methodSymbol.returnType
    val methodMirror = sourceInstance.reflectMethod(methodSymbol)

    args.size match {
      case 0 => methodComposition(methodName, methodMirror, result)
      case 1 => methodComposition(methodName, methodMirror, args.head, result)
      case 2 => methodComposition(methodName, methodMirror, args.head, args(1), result)
      case 3 => methodComposition(methodName, methodMirror, args.head, args(1), args(2), result)
      case i@ _ => println(s"not implemented $i"); new Composition(Map())
    }
  }

  private def methodComposition(methodName: String, methodMirror: MethodMirror, result: Type): Composition = {
    try {
      Composition(
        result -> (methodName, methodMirror()),
        function0(result) -> (methodName, () => methodMirror())
      )
    } catch {
      case NonFatal(_) => Composition()
    }
  }

  private def methodComposition(methodName: String, methodMirror: MethodMirror, arg1: Type, result: Type): Composition = {
    Composition(
      function1(arg1, result) -> (methodName, (a: Any) => methodMirror(a))
    )
  }

  private def methodComposition(methodName: String, methodMirror: MethodMirror, arg1: Type, arg2: Type, result: Type): Composition = {
    Composition(
      function2(arg1, arg2, result) -> (methodName + "($1, $2)", (a1: Any, a2: Any) => methodMirror(a1, a2)),
      function2(arg2, arg1, result) -> (methodName + "($2, $1)", (a2: Any, a1: Any) => methodMirror(a1, a2)),

      function1(arg1, function1(arg2, result)) -> (methodName + "($1, $2)", (a1: Any) => (a2: Any) => methodMirror(a1, a2)),
      function1(arg2, function1(arg1, result)) -> (methodName + "($2, $1)", (a2: Any) => (a1: Any) => methodMirror(a1, a2)),

      function1(tuple2(arg1, arg2), result) -> (methodName + "($1, $2)", (t: (Any, Any)) => methodMirror(t._1, t._2)),
      function1(tuple2(arg2, arg1), result) -> (methodName + "($2, $1)", (t: (Any, Any)) => methodMirror(t._2, t._1)),
    )
  }

  private def methodComposition(methodName: String, methodMirror: MethodMirror, arg1: Type, arg2: Type, arg3: Type, result: Type): Composition = {
    Composition(
      function1(arg1, function1(arg2, function1(arg3, result))) -> (methodName + "($1, $2, $3)", (a1: Any) => (a2: Any) => (a3: Any) => methodMirror(a1, a2, a3)),
      function1(arg1, function1(arg3, function1(arg2, result))) -> (methodName + "($1, $3, $2)", (a1: Any) => (a3: Any) => (a2: Any) => methodMirror(a1, a2, a3)),
      function1(arg2, function1(arg1, function1(arg3, result))) -> (methodName + "($2, $1, $3)", (a2: Any) => (a1: Any) => (a3: Any) => methodMirror(a1, a2, a3)),
      function1(arg2, function1(arg3, function1(arg1, result))) -> (methodName + "($2, $3, $1)", (a2: Any) => (a3: Any) => (a1: Any) => methodMirror(a1, a2, a3)),
      function1(arg3, function1(arg1, function1(arg2, result))) -> (methodName + "($3, $1, $2)", (a3: Any) => (a1: Any) => (a2: Any) => methodMirror(a1, a2, a3)),
      function1(arg3, function1(arg2, function1(arg1, result))) -> (methodName + "($3, $2, $1)", (a3: Any) => (a2: Any) => (a1: Any) => methodMirror(a1, a2, a3))
    )
  }

  private def function0(result: Type): Type = {
    val ExistentialType(_, TypeRef(pre, sym, _)) = typeOf[() => _]
    internal.typeRef(pre, sym, List(result))
  }

  private def function1(arg1: Type, result: Type): Type = {
    val ExistentialType(_, TypeRef(pre, sym, _)) = typeOf[_ => _]
    internal.typeRef(pre, sym, List(arg1, result))
  }

  private def function2(arg1: Type, arg2: Type, result: Type): Type = {
    val ExistentialType(_, TypeRef(pre, sym, _)) = typeOf[(_, _) => _]
    internal.typeRef(pre, sym, List(arg1, arg2, result))
  }

  private def tuple2(_1: Type, _2: Type): Type = {
    val ExistentialType(_, TypeRef(pre, sym, _)) = typeOf[(_, _)]
    internal.typeRef(pre, sym, List(_1, _2))
  }
}
