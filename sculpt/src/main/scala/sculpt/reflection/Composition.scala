package sculpt.reflection

import sculpt.source.{SafeImpl, Source, WeakImpl}

import scala.language.higherKinds
import scala.reflect.runtime.universe._

private[sculpt] class Composition(val composition: Map[Type, Seq[(String, Any)]]) extends Source {

  def merge(other: Composition): Composition = {
    val mergedDecomposition = other.composition.foldLeft(composition) {
      case (dec, (implType, seq)) =>
        dec.find(_._1 =:= implType).map {
          case (_, otherSeq) => dec.updated(implType, seq ++ otherSeq)
        }.getOrElse(dec + (implType -> seq))
    }
    new Composition(mergedDecomposition)
  }

  def find[A: TypeTag]: Seq[SafeImpl[A]] = {
    val implType = typeOf[A]
    for {
      (typeKey, seq) <- composition.toSeq
      if typeKey <:< implType
      (name, value) <- seq
    } yield SafeImpl(name, value.asInstanceOf[A])
  }

  def weakFind[A[_], T](implicit tt: WeakTypeTag[A[T]]): Seq[WeakImpl[A, T]] = {
    val fullType = weakTypeOf[A[T]]

    val ExistentialType((quantified, underlying)) = weakTypeOf[A[_]]
    val implType = internal.existentialType(quantified, replaceArgs(fullType, underlying.typeArgs.head))

    for {
      (typeKey, seq) <- composition.toSeq
      if typeKey <:< implType
      (name, value) <- seq
    } yield WeakImpl(name, value.asInstanceOf[A[T]])
  }

  def replaceArgs(fullType: Type, arg: Type): Type = {
    fullType match {
      case TypeRef(pre, sym, args) =>
        if (args.nonEmpty) {
          val here = sym.asType.name != TypeName("Function1")
          internal.typeRef(pre, sym, args.map(replaceArgs(_, arg)))
        } else if (sym.isParameter) arg else fullType

      case _ => fullType
    }
  }
}


private[reflection]  object Composition {
  def apply(components: (Type, (String, Any))*): Composition = {
    components.map { case(implType, impl) =>
      new Composition(Map(implType -> Seq(impl)))
    }.foldLeft(new Composition(Map()))(_.merge(_))
  }

}
