package sculpt.test

trait Specification {
  def run(reporter: SpecificationReporter): Status
}
