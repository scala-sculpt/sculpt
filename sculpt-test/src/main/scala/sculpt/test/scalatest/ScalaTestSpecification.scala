package sculpt.test.scalatest

import org.scalatest._
import org.scalatest.events._
import sculpt.test.{Specification, SpecificationReporter, Status}

class ScalaTestSpecification(suite: Suite) extends Specification {
  override def run(reporter: SpecificationReporter): Status = {
    val args = Args(new ScalaTestReporter(reporter))
    val specName = s"Specification ${suite.suiteName}"
    val specId = suite.suiteId

    reporter.specificationStarted(specName, specId)
    val status = suite.run(None, args)
    reporter.specificationCompleted()

    if (status.succeeds()) Status.Success else Status.Failure
  }

  private class ScalaTestReporter(reporter: SpecificationReporter) extends org.scalatest.Reporter {
    override def apply(event: Event): Unit = event match {
      case SuiteStarting(_, suiteName, suiteId, _, _, _, _, _, _, _) => reporter.specificationStarted(suiteName, suiteId)
      case SuiteCompleted(_, _, _, _, _, _, _, _, _, _, _) => reporter.specificationCompleted()
      case TestStarting(_, _, _, _, testName, _, _, _, _, _, _, _) => reporter.testStarted(testName)
      case TestFailed(_, errorMessage, _, _, _, _, _, _, _, _, _, _, _, _, _, _) => reporter.testFailed(errorMessage)
      case TestSucceeded(_, _, _, _, _, _, _, _, _, _, _, _, _, _) => reporter.testSucceeded()
      case _ =>
    }
  }


}
