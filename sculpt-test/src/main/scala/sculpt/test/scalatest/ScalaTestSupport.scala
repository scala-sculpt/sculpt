package sculpt.test.scalatest

import sculpt.test.{Specification, Support}
import sculpt.scalatest.Spec


trait ScalaTestSupport {
  implicit val support: Support[Spec] = new Support[Spec] {
    override def apply[A](spec: Spec[A])(a: A): Specification = new ScalaTestSpecification(spec(a))
  }
}
