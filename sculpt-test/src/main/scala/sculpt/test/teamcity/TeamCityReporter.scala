package sculpt.test.teamcity

import sculpt.test.Reporter

object TeamCityReporter extends Reporter {
  private final val rootId: String = "0"

  override def suiteStarted(name: String, id: String): Unit = {
    System.out.println(s"##teamcity[testSuiteStarted name='${escape(name)}' captureStandardOutput='true' nodeId='${escape(id)}' parentNodeId='${escape(rootId)}']")
  }

  override def suiteCompleted(name: String, id: String): Unit = {
    System.out.println(s"##teamcity[testSuiteFinished name='${escape(name)}' nodeId='${escape(id)}']")
  }

  override def nodeStarted(name: String, id: String, parentId: String): Unit = {
    System.out.println(s"##teamcity[testStarted name='${escape(name)}' captureStandardOutput='true' nodeId='${escape(id)}' parentNodeId='${escape(parentId)}']")
  }

  override def nodeSucceeded(name: String, id: String): Unit = {
    System.out.println(s"##teamcity[testFinished name='${escape(name)}' nodeId='${escape(id)}']")
  }

  override def nodeFailed(name: String, id: String, errorMessage: String): Unit = {
    System.out.println(s"##teamcity[testFailed name='${escape(name)}' message='${escape(errorMessage)}' nodeId='${escape(id)}']")
    System.out.println(s"##teamcity[testFinished name='${escape(name)}' nodeId='${escape(id)}']")
  }

  private def escape(str: String): String = {
    str.replaceAll("[|]", "||")
      .replaceAll("[']", "|'")
      .replaceAll("[\n]", "|n")
      .replaceAll("[\r]", "|r")
      .replaceAll("]", "|]")
      .replaceAll("\\[", "|[")
  }
}
