package sculpt.test

import sculpt.source.Impl

sealed trait ExecutionTree[+A] {
  def run(reporter: ExecutionContext): Status
  def map[B](f: A => B): ExecutionTree[B]
  def flatMap[B](f: Impl[A] => ExecutionTree[B]): ExecutionTree[B]
  def satisfying(spec: A => Specification): ExecutionTree[A]
}

object ExecutionTree {
  final case class Node[A, B](potential: String, candidates: Seq[Candidate[A]], child: Impl[A] => ExecutionTree[B]) extends ExecutionTree[B] {
    override def run(context: ExecutionContext): Status = {
      if (candidates.isEmpty) {
       context.given(potential)(_ => Status.Failure)
      } else {
        candidates.map(candidate => context.given(potential, candidate)(child(candidate.impl).run))
          .find(_ == Status.Success)
          .getOrElse(Status.Failure)
      }
    }

    override def map[C](f: B => C): ExecutionTree[C] = Node(potential, candidates, (impl: Impl[A]) => child(impl).map(f))
    override def flatMap[C](g: Impl[B] => ExecutionTree[C]): ExecutionTree[C] = Node(potential, candidates, (impl: Impl[A]) => child(impl).flatMap(g))
    override def satisfying(spec: B => Specification): ExecutionTree[B] = Node(potential, candidates, (impl: Impl[A]) => child(impl).satisfying(spec))
  }

  final case class Leaf[A](potential: String, candidates: Seq[Candidate[A]]) extends ExecutionTree[A] {
    override def run(context: ExecutionContext): Status = {
      if (candidates.isEmpty) {
        context.given(potential)(_ => Status.Failure)
      } else {
        candidates.map(context.given(potential, _)(_ => Status.Success))
          .find(_ == Status.Success)
          .getOrElse(Status.Failure)
      }
    }

    override def map[B](f: A => B): ExecutionTree[B] = Leaf(potential, candidates.map(_.map(f)))
    override def flatMap[B](f: Impl[A] => ExecutionTree[B]): ExecutionTree[B] = Node(potential, candidates, f)
    override def satisfying(spec: A => Specification): ExecutionTree[A] = Leaf(potential, candidates.map(_.satisfying(spec)))
  }
}
