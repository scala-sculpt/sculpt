package sculpt.test

import scala.language.higherKinds

trait Support[Spec[_]] {
  def apply[A](spec: Spec[A])(a: A): Specification
}
