package sculpt.test

import scala.collection.mutable

trait ExecutionContext {
  def given(potential: String)(exec: ExecutionContext => Status): Status
  def given(potential: String, candidate: Candidate[Any])(exec: ExecutionContext => Status): Status
}

class RootContext(suiteId: String, reporter: Reporter) extends ExecutionContext {
  private val potentials = mutable.Map[Potential, Status]()

  private case class Potential(name: String, id: String)

  def given(potential: String)(exec: ExecutionContext => Status): Status = {
    in(Potential(s"Potential $potential", s"$suiteId[$potential]")) {
      exec(this)
    }
  }

  override def given(potential: String, candidate: Candidate[Any])(exec: ExecutionContext => Status): Status = {
    val childContext = new ChildContext(Seq(potential), Seq(candidate.name))
    childContext.run(candidate) andThen exec(childContext)
  }

  def in(potential: Potential)(exec: => Status): Status = {
    if (!potentials.contains(potential)) {
      reporter.nodeStarted(potential.name, potential.id, suiteId)
      potentials += (potential -> Status.Failure)
    }
    val status = exec
    status match {
      case Status.Success => potentials.update(potential, Status.Success)
      case Status.Failure => ()
    }
    status
  }

  private  class ChildContext(potentials: Seq[String], candidates: Seq[String]) extends ExecutionContext {
    private val candidateName = "Candidate " + candidates.mkString(" -> ")
    private val candidateId = s"$suiteId[${candidates.mkString(" -> ")}]"
    private val potentialName = s"Potential ${potentials.last}"
    private val potentialId = s"$suiteId[${potentials.mkString(" -> ")}]"

    def given(potential: String)(exec: ExecutionContext => Status): Status = {
      in(Potential(s"Potential $potential", s"$suiteId[${(potentials :+ potential).mkString(" -> ")}]")) {
        exec(this)
      }
    }

    override def given(potential: String, candidate: Candidate[Any])(exec: ExecutionContext => Status): Status = {
      val childContext = new ChildContext(potentials :+ potential, candidates :+ candidate.name)
      childContext.run(candidate) andThen exec(childContext)
    }

    def run(candidate: Candidate[Any]): Status = {
      in (Potential(potentialName, potentialId)) {
        reporter.nodeStarted(candidateName, candidateId, potentialId)
        val specReporter = new SpecificationReporter(reporter, candidateId)
        val status = candidate.run(specReporter)
        status match {
          case Status.Success => reporter.nodeSucceeded(candidateName, candidateId)
          case Status.Failure => reporter.nodeFailed(candidateName, candidateId, "")
        }
        status
      }
    }
  }

  def close(): Unit = {
    for ((potential, status) <- potentials) {
      status match {
        case Status.Success => reporter.nodeSucceeded(potential.name, potential.id)
        case Status.Failure => reporter.nodeFailed(potential.name, potential.id, "")
      }
    }
  }
}
