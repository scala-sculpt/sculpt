package sculpt.test.runner

import java.io.{File, IOException}
import java.util.jar.JarFile

import sculpt.test.SculptSuite

object SuiteDiscoveryHelper {
  val fileSeparator: Char = {
    val property = System.getProperty("path.separator")
    property.headOption.getOrElse(':')
  }

  def findSuites(path: String, loader: ClassLoader): Set[String] = {
    for {
      fileName <- findFileNames(path) if fileName.endsWith(".class")
      className = fileName.dropRight(6).replace('/', '.')
      if isSuite(className, loader)
    } yield className
  }

  private def isSuite(className: String, loader: ClassLoader): Boolean = {
    println(className)
    try {
      val clazz = loader.loadClass(className)
      classOf[SculptSuite].isAssignableFrom(clazz)
    } catch {
      case cause: NoClassDefFoundError =>
        println(cause.getMessage)
        false
    }
  }

  private def findFileNames(path: String): Set[String] = {
    if (path.endsWith(".jar")) {
      for {
        jar <- tryGetJar(path).toSet[JarFile]
        fileName <- findFileNamesInJar(jar)
      } yield fileName
    } else {
      findFileNamesInFile(new File(path))
    }
  }

  private def tryGetJar(path: String): Option[JarFile] = {
    try {
      Some(new JarFile(path))
    } catch {
      case _: IOException => None
    }
  }

  private def findFileNamesInJar(jar: JarFile): Set[String] = {
    val entries = jar.entries()

    new Iterator[String] {
      def hasNext: Boolean = entries.hasMoreElements
      def next(): String = entries.nextElement().getName
    }.toSet
  }

  private def findFileNamesInFile(file: File): Set[String] = {
    def recursively(file: File): Set[String] = {
      if (file.isDirectory) {
        val directoryName = file.getName
        for {
          child <- file.listFiles.toSet[File]
          fileName <- recursively(child)
        } yield s"$directoryName/$fileName"
      } else {
        Set(file.getName)
      }
    }

    if (file.isDirectory) {
      for {
        child <- file.listFiles.toSet[File]
        fileName <- recursively(child)
      } yield fileName
    } else {
      Set(file.getName)
    }
  }
}
