package sculpt.test

class SpecificationReporter(reporter: Reporter, rootId: String) {
  private var nodeStates = List[NodeState]()

  def specificationStarted(name: String, id: String): Unit = {
    val parentId = if (nodeStates.isEmpty) rootId else nodeStates.head.id
    val uniqueId = s"$rootId.$id"
    reporter.nodeStarted(name, uniqueId, parentId)
    nodeStates = new NodeState(name, uniqueId)::nodeStates
  }

  def specificationCompleted(): Unit = {
    val state = nodeStates.head
    state.errorMessage match {
      case None => reporter.nodeSucceeded(state.name, state.id)
      case Some(errorMessage) => reporter.nodeFailed(state.name, state.id, errorMessage)
    }
    nodeStates = nodeStates.tail
  }

  def testStarted(name: String): Unit = {
    val parentId = if (nodeStates.isEmpty) rootId else nodeStates.head.id
    val uniqueId = s"$parentId[$name]"
    reporter.nodeStarted(name, uniqueId, parentId)
    nodeStates = new NodeState(name, uniqueId)::nodeStates
  }

  def testFailed(message: String): Unit = {
    nodeStates.foreach(_.failed(message))
    val state = nodeStates.head
    reporter.nodeFailed(state.name, state.id, message)
    nodeStates = nodeStates.tail
  }

  def testSucceeded(): Unit = {
    val state = nodeStates.head
    reporter.nodeSucceeded(state.name, state.id)
    nodeStates = nodeStates.tail
  }

  private class NodeState(val name: String, val id: String) {
    var errorMessage = Option.empty[String]
    def failed(message: String): Unit = {
      errorMessage = errorMessage.map(m => s"$m\n$message").orElse(Some(message))
    }
  }
}
