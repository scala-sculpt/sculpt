package sculpt.test

import sculpt.source.Source

import scala.collection.mutable.ListBuffer
import scala.language.higherKinds

abstract class SculptSuite(source: Source) {
  private val executionTrees = ListBuffer.empty[ExecutionTree[Any]]

  def test[A, Spec[_]: Support](potential: sculpt.Potential[A, Spec]): Unit = {
    executionTrees += potential.resolveWith(new TestSolver(source))
  }

  def run(context: ExecutionContext): Status = executionTrees.map(_.run(context)).fold(Status.Success)(_ & _)
}
