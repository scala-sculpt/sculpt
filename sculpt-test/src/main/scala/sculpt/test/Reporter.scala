package sculpt.test

trait Reporter {
  def suiteStarted(name: String, id: String): Unit
  def suiteCompleted(name: String, id: String): Unit

  def nodeStarted(name: String, id: String, parentId: String): Unit
  def nodeSucceeded(name: String, id: String): Unit
  def nodeFailed(name: String, id: String, errorMessage: String): Unit
}
