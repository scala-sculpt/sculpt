package sculpt.test

import java.io.{File, IOException}
import java.net.{URL, URLClassLoader}

import scopt.OptionParser
import sculpt.test.runner.SuiteDiscoveryHelper
import sculpt.test.teamcity.TeamCityReporter

object SculptRunner {
  private case class Config(suites: Seq[String], paths: Seq[String])
  private object Config {
    def apply(): Config = Config(Seq.empty[String], Seq.empty[String])
  }

  private val parser = new OptionParser[Config](getClass.getName) {
    opt[Seq[String]]('s', "suites").action { (suites, config) => config.copy(suites = suites) }
    opt[Seq[String]]('p', "paths").action( (paths, config) => config.copy(paths = paths))
  }

  def run(args: Array[String]): Unit = {
    for (config <- parser.parse(args, Config())) {
      if(config.paths.nonEmpty) {
        val loader = getClassLoader(config.paths)
        val suites = config.paths.flatMap(path => SuiteDiscoveryHelper.findSuites(path, loader))
        println(suites)
      }

      if(config.suites.nonEmpty)
        config.suites.toSet.foreach(runSuite)
    }
  }

  private def getClassLoader(paths: Seq[String]): ClassLoader = {
    val urls = paths.map(getURL).toArray
    for (url <- urls) try {
      url.openConnection().setDefaultUseCaches(false)
    } catch {
      case _: IOException =>
    }

    new URLClassLoader(urls, classOf[SculptSuite].getClassLoader)
  }

  private def getURL(path: String): URL = {
    new File(path).toURI.toURL
  }


  private def runSuite(suite: String): Unit = {
    val suiteClass = getClass.getClassLoader.loadClass(suite)
    val suiteName = suiteClass.getSimpleName
    val suiteId = suiteClass.getCanonicalName
    val reporter = TeamCityReporter
    reporter.suiteStarted(suiteName, suiteId)
    val context = new RootContext(suiteId, TeamCityReporter)
    val status = suiteClass.getConstructor().newInstance().asInstanceOf[SculptSuite].run(context)
    context.close()
    status match {
      case Status.Success => reporter.nodeSucceeded(suiteName, suiteId)
      case Status.Failure => reporter.nodeFailed(suiteName, suiteId, "")
    }
  }
}
