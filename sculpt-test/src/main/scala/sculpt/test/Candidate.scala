package sculpt.test

import sculpt.source.Impl

final case class Candidate[+A](impl: Impl[A], specs: Seq[Specification]) {
  val name: String = impl.name
  def run(reporter: SpecificationReporter): Status = specs.map(_.run(reporter)).fold(Status.Success)(_ & _)
  def map[B](f: A => B): Candidate[B] = Candidate(impl.map(f), specs)
  def satisfying(spec: A => Specification): Candidate[A] = Candidate(impl, specs :+ spec(impl.value))
}
