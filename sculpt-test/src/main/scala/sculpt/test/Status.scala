package sculpt.test


sealed trait Status {
  def &(other: Status): Status
  def andThen(other: => Status): Status
}

object Status {
  def success: Status = Success

  final case object Success extends Status {
    override def &(other: Status): Status = other
    override def andThen(other: => Status): Status = other
  }

  final case object Failure extends Status {
    override def &(other: Status): Status = this
    override def andThen(other: => Status): Status = this
  }
}
