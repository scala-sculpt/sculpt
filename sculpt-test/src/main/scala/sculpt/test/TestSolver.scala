package sculpt.test

import sculpt.source.{Impl, Source}
import sculpt.Solver

import scala.language.higherKinds
import scala.reflect.runtime.universe.{TypeTag, WeakTypeTag, typeOf, weakTypeOf}

class TestSolver[Spec[_]: Support](source: Source) extends Solver[ExecutionTree, Spec] {
  override def any[A: TypeTag]: ExecutionTree[A] = {
    val potential = typeOf[A].toString
    val candidates = source.find[A].map {
      impl => Candidate(impl, Seq())
    }
    ExecutionTree.Leaf(potential, candidates)
  }

  override def weakAny[A[_], T](implicit tt: WeakTypeTag[A[T]]): ExecutionTree[A[T]] = {
    val potential = weakTypeOf[A[T]].toString
    val candidates: Seq[Candidate[A[T]]] = source.weakFind[A, T].map {
      impl => Candidate(impl, Seq())
    }
    ExecutionTree.Leaf[A[T]](potential, candidates)
  }

  override def map[A, B](tree: ExecutionTree[A])(f: A => B): ExecutionTree[B] = tree.map(f)

  override def flatMap[A, B](tree: ExecutionTree[A])(f: Impl[A] => ExecutionTree[B]): ExecutionTree[B] = tree.flatMap(f)

  override def satisfying[A](tree: ExecutionTree[A])(spec: Spec[A]): ExecutionTree[A] = {
    val support = implicitly[Support[Spec]]
    tree.satisfying(support(spec))
  }

  override def given[A](impl: Impl[A]): Solver[ExecutionTree, Spec] = {
    new TestSolver(source.given(impl))
  }
}
