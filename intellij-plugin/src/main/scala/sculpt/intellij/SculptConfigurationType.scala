package sculpt.intellij

import com.intellij.execution.configurations.{ConfigurationFactory, ConfigurationType}
import javax.swing.Icon

class SculptConfigurationType extends ConfigurationType {
  val configurationFactory = new SculptRunConfigurationFactory(this)

  override def getDisplayName: String = Sculpt.display

  override def getConfigurationTypeDescription: String = "Specification testing framework run configuration"

  override def getIcon: Icon = Icons.specification

  override def getId: String = getClass.getSimpleName

  override def getConfigurationFactories: Array[ConfigurationFactory] = Array(configurationFactory)
}
