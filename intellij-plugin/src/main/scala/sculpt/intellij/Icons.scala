package sculpt.intellij

import com.intellij.openapi.util.IconLoader
import javax.swing.Icon

object Icons {
  val specification: Icon = IconLoader.getIcon("/sculpt/images/sculpt_icon.svg")
}
