package sculpt.intellij

import java.io.{File, FileOutputStream, IOException, PrintStream}

import com.intellij.execution.configurations._
import com.intellij.execution.impl.ConsoleViewImpl
import com.intellij.execution.runners.{ExecutionEnvironment, ProgramRunner}
import com.intellij.execution.testDiscovery.JavaAutoRunManager
import com.intellij.execution.testframework.TestFrameworkRunningModel
import com.intellij.execution.testframework.autotest.{AbstractAutoTestManager, ToggleAutoTestAction}
import com.intellij.execution.testframework.sm.SMTestRunnerConnectionUtil
import com.intellij.execution.testframework.sm.runner.SMTRunnerConsoleProperties
import com.intellij.execution.testframework.sm.runner.ui.SMTRunnerConsoleView
import com.intellij.execution.testframework.ui.BaseTestsOutputConsoleView
import com.intellij.execution.{DefaultExecutionResult, ExecutionException, ExecutionResult, Executor, JavaRunConfigurationExtensionManager, RunConfigurationExtension}
import com.intellij.openapi.components.PathMacroManager
import com.intellij.openapi.project.Project
import com.intellij.openapi.projectRoots.{JdkUtil, ProjectJdkTable}
import com.intellij.openapi.util.Getter
import com.intellij.openapi.util.text.StringUtil
import com.intellij.openapi.vfs.newvfs.ManagingFS
import com.intellij.openapi.vfs.newvfs.persistent.PersistentFSImpl
import com.intellij.util.PathUtil
import org.jetbrains.plugins.scala.statistics.{FeatureKey, Stats}
import org.jetbrains.plugins.scala.testingSupport.locationProvider.ScalaTestLocationProvider
import org.jetbrains.plugins.scala.testingSupport.test.{AbstractTestRerunFailedTestsAction, AbstractTestRunConfiguration}
import org.jetbrains.plugins.scala.testingSupport.test.AbstractTestRunConfiguration.SettingMap
import org.jetbrains.plugins.scala.testingSupport.test.TestRunConfigurationForm.SearchForTest
import org.jetbrains.plugins.scala.testingSupport.test.sbt.{SbtProcessHandlerWrapper, SbtTestEventHandler}
import org.jetbrains.sbt.SbtUtil
import org.jetbrains.sbt.shell.{SbtProcessManager, SbtShellCommunication, SettingQueryHandler}
import scopt.OptionParser
import sculpt.test.SculptRunner
import sculpt.test.teamcity.TeamCityReporter

import scala.concurrent.Future

class SculptRunConfiguration(project: Project, configurationFactory: ConfigurationFactory, name: String)
  extends AbstractTestRunConfiguration(project, configurationFactory, name, SculptRunConfiguration.producer) {
  override def suitePaths: List[String] = List(Sculpt.Suite.path)

  override def mainClass: String = classOf[MainRunner].getName

  override def reporterClass: String = TeamCityReporter.getClass.getName

  override def errorMessage: String = "Sculpt is not specified"

  override def getState(executor:  Executor, env: ExecutionEnvironment): RunProfileState = {
    new JavaCommandLineState(env) {
      private def suitesToTestsMap: Map[String, Set[String]] = testConfigurationData.getTestMap()

      val getClasses: Seq[String] = testConfigurationData.getTestMap().keys.toSeq

      protected override def createJavaParameters: JavaParameters = {
        import org.jetbrains.plugins.scala.project._
        import scala.collection.JavaConverters._

        val module = getModule

        val params = new JavaParameters()

        params.setCharset(null)
        var vmParams = getJavaOptions

        //expand macros
        vmParams = PathMacroManager.getInstance(project).expandPath(vmParams)
        vmParams = PathMacroManager.getInstance(module).expandPath(vmParams)

        val mutableEnvVariables = getEnvVariables
        params.setEnv(mutableEnvVariables)

        //expand environment variables in vmParams
        params.getEnv.entrySet.forEach { entry =>
          vmParams = StringUtil.replace(vmParams, "$" + entry.getKey + "$", entry.getValue, false)
        }

        params.getVMParametersList.addParametersString(vmParams)
        val wDir = getWorkingDirectory
        params.setWorkingDirectory(expandPath(wDir))

        for (clazz <-Set(classOf[MainRunner], SculptRunner.getClass, classOf[OptionParser[_]])) {
          val jar = PathUtil.getJarPathForClass(clazz)
          params.getClassPath.add(jar)
        }

        ManagingFS.getInstance match {
          case fs: PersistentFSImpl => fs.incStructuralModificationCount()
          case _                    =>
        }

        val maybeCustomSdk = for {
          path <- Option(jrePath)
          jdk  <- Option(ProjectJdkTable.getInstance().findJdk(path))
        } yield jdk

        searchTest match {
          case SearchForTest.IN_WHOLE_PROJECT =>
            val sdk = maybeCustomSdk.orElse(
              project
                .modules
                .iterator.map(JavaParameters.getValidJdkToRunModule(_, false))
                .collectFirst { case jdk if jdk ne null => jdk }
            )
            params.configureByProject(project, JavaParameters.JDK_AND_CLASSES_AND_TESTS, sdk.orNull)
          case _ =>
            val sdk = maybeCustomSdk.getOrElse(JavaParameters.getValidJdkToRunModule(module, false))
            params.configureByModule(module, JavaParameters.JDK_AND_CLASSES_AND_TESTS, sdk)
        }

        params.setMainClass(mainClass)

        val (myAdd, myAfter): (String => Unit, () => Unit) =
          if (JdkUtil.useDynamicClasspath(getProject)) {
            try {
              val fileWithParams: File = File.createTempFile("abstracttest", ".tmp")
              val outputStream = new FileOutputStream(fileWithParams)
              val printer: PrintStream = new PrintStream(outputStream)
              params.getProgramParametersList.add("@" + fileWithParams.getPath)

              (printer.println(_:String), () => printer.close())
            }
            catch {
              case ioException: IOException => throw new ExecutionException("Failed to create dynamic classpath file with command-line args.", ioException)
            }
          } else {
            (params.getProgramParametersList.add(_), () => ())
          }

        ParametersList.parse(getTestArgs).foreach(myAdd)
        addClassesAndTests(suitesToTestsMap, myAdd)
        myAfter()

        for (ext <- RunConfigurationExtension.EP_NAME.getExtensionList.asScala) {
          ext.updateJavaParameters(currentConfiguration, params, getRunnerSettings)
        }

        params
      }

      override def execute(executor: Executor, runner: ProgramRunner[_ <: RunnerSettings]): ExecutionResult = {
        import scala.concurrent.ExecutionContext.Implicits.global

        val processHandler = if (useSbt) {
          //use a process running sbt
          val sbtProcessManager = SbtProcessManager.forProject(project)
          //make sure the process is initialized
          val shellRunner = sbtProcessManager.acquireShellRunner
          SbtProcessHandlerWrapper(shellRunner createProcessHandler null)
        } else startProcess()
        val runnerSettings = getRunnerSettings
        JavaRunConfigurationExtensionManager.getInstance.
          attachExtensionsToProcess(currentConfiguration, processHandler, runnerSettings)
        val consoleProperties: SMTRunnerConsoleProperties = new SMTRunnerConsoleProperties(currentConfiguration, "Scala", executor) {
          override def getTestLocator = new ScalaTestLocationProvider
        }

        consoleProperties.setIdBasedTestTree(true)

        // console view
        val consoleView = if (useSbt && !useUiWithSbt) {
          val console = new ConsoleViewImpl(project, true)
          console.attachToProcess(processHandler)
          console
        } else SMTestRunnerConnectionUtil.createAndAttachConsole("Scala", processHandler, consoleProperties)

        val res = new DefaultExecutionResult(
          consoleView, processHandler,
          createActions(consoleView, processHandler, executor): _*)

        consoleView match {
          case testConsole: BaseTestsOutputConsoleView =>
            val rerunFailedTestsAction = new AbstractTestRerunFailedTestsAction(testConsole)
            rerunFailedTestsAction.init(testConsole.getProperties)
            rerunFailedTestsAction.setModelProvider(new Getter[TestFrameworkRunningModel] {
              def get: TestFrameworkRunningModel = {
                testConsole.asInstanceOf[SMTRunnerConsoleView].getResultsViewer
              }
            })
            res.setRestartActions(rerunFailedTestsAction, new ToggleAutoTestAction() {
              override def isDelayApplicable: Boolean = false

              override def getAutoTestManager(project: Project): AbstractAutoTestManager = JavaAutoRunManager.getInstance(project)
            })
          case _ =>
        }
        if (useSbt) {
          Stats.trigger(FeatureKey.sbtShellTestRunConfig)

          val commands = buildSbtParams(suitesToTestsMap).
            map(SettingQueryHandler.getProjectIdPrefix(SbtUtil.getSbtProjectIdSeparated(getModule)) + "testOnly" + _)
          val comm = SbtShellCommunication.forProject(project)
          val handler = new SbtTestEventHandler(processHandler)

          val sbtRun = {
            lazy val oldSettings = if (useUiWithSbt)
              for {
                _ <- initialize(comm)
                mod <- modifySbtSettingsForUi(comm)
              } yield mod
            else Future.successful(SettingMap())

            lazy val cmdF = commands.map(
              comm.command(_, {}, SbtShellCommunication.listenerAggregator(handler), showShell = false)
            )
            for {
              old <- oldSettings
              _ <- Future.sequence(cmdF)
              reset <- resetSbtSettingsForUi(comm, old)
            } yield reset
          }
          sbtRun.onComplete(_ => handler.closeRoot())
        }
        res
      }
    }
  }
}

object SculptRunConfiguration {
  val producer = new SculptConfigurationProducer
}
