package sculpt.intellij

import sculpt.test.SculptSuite

object Sculpt {
  final val display: String = "Sculpt"

  object Suite {
    final val display: String = "Sculpt Suite"
    final val path: String = classOf[SculptSuite].getCanonicalName
  }
}
