package sculpt.intellij

import org.jetbrains.plugins.scala.testingSupport.test.AbstractTestFramework

class SculptTestFramework extends AbstractTestFramework {
  override def getTestFileTemplateName: String = Sculpt.Suite.display

  override protected def getLibraryDependencies(scalaVersion: Option[String]): Seq[String] = Seq()

  override protected def getLibraryResolvers(scalaVersion: Option[String]): Seq[String] = Seq()

  override protected def getAdditionalBuildCommands(scalaVersion: Option[String]): Seq[String] = Seq()

  override def getSuitePaths: Seq[String] = Seq(Sculpt.Suite.path)

  override def getMarkerClassFQName: String = Sculpt.Suite.path

  override def getMnemonic: Char = 's'

  override def getName: String = Sculpt.display

  override def getDefaultSuperClass: String = Sculpt.Suite.path
}
