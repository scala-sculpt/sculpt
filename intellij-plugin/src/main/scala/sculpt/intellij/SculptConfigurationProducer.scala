package sculpt.intellij

import com.intellij.execution.configurations.RunConfiguration
import com.intellij.execution.junit.JavaRuntimeConfigurationProducerBase
import com.intellij.execution.{JavaRunConfigurationExtensionManager, Location, RunManager, RunnerAndConfigurationSettings}
import com.intellij.openapi.util.text.StringUtil
import com.intellij.psi.util.PsiTreeUtil
import com.intellij.psi.{JavaDirectoryService, PsiDirectory, PsiElement, PsiPackage}
import org.jetbrains.plugins.scala.lang.psi.ScalaPsiUtil
import org.jetbrains.plugins.scala.lang.psi.api.base.ScLiteral
import org.jetbrains.plugins.scala.lang.psi.api.base.patterns.ScBindingPattern
import org.jetbrains.plugins.scala.lang.psi.api.expr.{MethodInvocation, ScReferenceExpression}
import org.jetbrains.plugins.scala.lang.psi.api.toplevel.typedef.{ScClass, ScMember, ScTrait, ScTypeDefinition}
import org.jetbrains.plugins.scala.testingSupport.test.TestConfigurationUtil.isInheritor
import org.jetbrains.plugins.scala.testingSupport.test._

class SculptConfigurationProducer extends  {
  val configurationType = new SculptConfigurationType
} with TestConfigurationProducer(configurationType) {
  override def suitePaths: List[String] = List(Sculpt.Suite.path)

  override def createConfigurationByLocation(location: Location[_ <: PsiElement]): Option[(PsiElement, RunnerAndConfigurationSettings)] = {
    Option(location.getPsiElement).flatMap { element =>
      element match {
        case p: Package =>
          Some((p, packageSettings(p, location, configurationType.configurationFactory, p.getName)))
        case d: PsiDirectory =>
          Some((d, packageSettings(d, location, configurationType.configurationFactory, d.getName)))
        case _ =>
          val (nullableTestClass, testName) = getLocationClassAndTest(location)
          Option(nullableTestClass).map { testClass =>
            val testClassPath = testClass.qualifiedName
            val testClassName = StringUtil.getShortName(testClassPath)
            val configurationName = Option(testName).map(name => s"$testClassName.$name").getOrElse(testClassName)
            val settings = RunManager.getInstance(location.getProject)
              .createConfiguration(configurationName, configurationType.configurationFactory)

            val runConfiguration = settings.getConfiguration.asInstanceOf[SculptRunConfiguration]
            runConfiguration.initWorkingDir()
            runConfiguration.setTestConfigurationData(ClassTestData(runConfiguration, testClassPath, testName))

            Option(ScalaPsiUtil.getModule(element)).foreach(runConfiguration.setModule)

            JavaRunConfigurationExtensionManager.getInstance.extendCreatedConfiguration(runConfiguration, location)
            (testClass, settings)
          }
      }
    }
  }

  override def isConfigurationByLocation(configuration: RunConfiguration, location: Location[_ <: PsiElement]): Boolean = {
    Option(location.getPsiElement).exists { element =>
      element match {
        case _: PsiPackage | _: PsiDirectory =>
          configuration.isInstanceOf[SculptRunConfiguration] && isPackageConfiguration(element, configuration)
        case _ =>
          val (testClass, testName) = getLocationClassAndTest(location)
          if (testClass == null) return false
          val testClassPath = testClass.qualifiedName
          configuration match {
            case configuration: SculptRunConfiguration =>
              configuration.testConfigurationData match {
                case testData: SingleTestData => testData.testClassPath == testClassPath && testData.testName == testName
                case classData: ClassTestData => classData.testClassPath == testClassPath && testName == null
                case _ => false
              }
            case _ => false
          }
      }
    }
  }

  private def packageSettings(element: PsiElement, location: Location[_ <: PsiElement],
                              confFactory: AbstractTestRunConfigurationFactory,
                              name: String): RunnerAndConfigurationSettings = {
    val displayName = s"Specifications in '$name'"
    val pack: PsiPackage = element match {
      case dir: PsiDirectory => JavaRuntimeConfigurationProducerBase.checkPackage(dir)
      case pack: PsiPackage => pack
    }
    if (pack == null) return null
    val settings = RunManager.getInstance(location.getProject).createConfiguration(displayName, confFactory)
    val configuration = settings.getConfiguration.asInstanceOf[AbstractTestRunConfiguration]
    configuration.setTestConfigurationData(AllInPackageTestData(configuration, pack.getQualifiedName))
    configuration.setGeneratedName(displayName)
    configuration.setModule(location.getModule)
    configuration.initWorkingDir()
    JavaRunConfigurationExtensionManager.getInstance.extendCreatedConfiguration(configuration, location)
    settings
  }

  private def isPackageConfiguration(element: PsiElement, configuration: RunConfiguration): Boolean = {
    val pack: PsiPackage = element match {
      case dir: PsiDirectory => JavaDirectoryService.getInstance.getPackage(dir)
      case pack: PsiPackage => pack
    }
    if (pack == null) return false
    configuration match {
      case configuration: AbstractTestRunConfiguration =>
        configuration.getTestConfigurationData.isInstanceOf[AllInPackageTestData] &&
          configuration.getTestPackagePath == pack.getQualifiedName
      case _ => false
    }
  }

  override protected def getLocationClassAndTestImpl(location: Location[_ <: PsiElement]): (ScTypeDefinition, String) = {
    Option(location.getPsiElement).flatMap { element =>
      Option(PsiTreeUtil.getParentOfType(element, classOf[ScTypeDefinition], false)).flatMap {
        clazz =>
          def parent(clazz: ScTypeDefinition): ScTypeDefinition = {
            Option(PsiTreeUtil.getParentOfType(clazz, classOf[ScTypeDefinition], true)).map(parent).getOrElse(clazz)
          }

          parent(clazz) match {
            case parentClazz @ (_: ScClass | _: ScTrait) if isInheritor(parentClazz, Sculpt.Suite.path) =>
              val call = PsiTreeUtil.getParentOfType(element, classOf[MethodInvocation], false)
              Some((parentClazz, checkCallGeneral(call).orNull))
            case _ => None
          }
      }
    }.getOrElse((null, null))
  }

  private val namesSet = Map("test" -> Set(Sculpt.Suite.path))

  private def checkCallGeneral(nullableCall: MethodInvocation): Option[String] = {
    Option(nullableCall).flatMap { call =>
      (call.getInvokedExpr match {
        case ref: ScReferenceExpression if namesSet.isDefinedAt(ref.refName) =>
          Option(ref.resolve()).flatMap { resolve =>
            (resolve match {
              case fun: ScMember => Some(fun.containingClass)
              case p: ScBindingPattern =>
                p.nameContext match {
                  case v: ScMember => Some(v.containingClass)
                  case _ => None
                }
              case _ => None
            }).filter { containingClass =>
              namesSet(ref.refName).exists(fqn => fqn == containingClass.qualifiedName || isInheritor(containingClass, fqn))
            }.map(_ => inv(call))
          }
        case invokedCall: MethodInvocation => Some(checkCallGeneral(invokedCall))
        case _ => None
      }).getOrElse(checkCallGeneral(PsiTreeUtil.getParentOfType(call, classOf[MethodInvocation], true)))
    }
  }

  private def inv(call: MethodInvocation): Option[String] ={
    call.argumentExpressions.head match {
      case l: ScLiteral if l.isString =>
        Some(l.getValue.asInstanceOf[String])
      case _ => None
    }
  }
}
