package sculpt.intellij

import com.intellij.execution.configurations.{ConfigurationType, RunConfiguration}
import com.intellij.openapi.project.Project
import org.jetbrains.plugins.scala.testingSupport.test.AbstractTestRunConfigurationFactory

class SculptRunConfigurationFactory(typez: ConfigurationType) extends AbstractTestRunConfigurationFactory(typez) {
  override def createTemplateConfiguration(project: Project): RunConfiguration =
    new SculptRunConfiguration(project, configurationFactory = this, name = "")
}
