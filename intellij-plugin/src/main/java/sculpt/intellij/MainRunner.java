package sculpt.intellij;

import sculpt.test.SculptRunner;

public class MainRunner {
    public static void main(String[] args) {
        SculptRunner.run(args);
    }
}
