package axiom

import org.scalacheck.Gen

class Axiom[GIVEN, ASSERTION](val given: Gen[GIVEN], val test: GIVEN => ASSERTION) extends Given[ASSERTION](given.map(test))
