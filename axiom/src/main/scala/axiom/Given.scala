package axiom

import org.scalacheck.Gen
import org.scalatestplus.scalacheck.ScalaCheckDrivenPropertyChecks

class Given[A](val gen: Gen[A]) extends ScalaCheckDrivenPropertyChecks {

  def flatMap[B, C](f: A => Axiom[B, C]): Axiom[(A, B), C] = {
    val genAB = for {
      a <- gen
      b <- f(a).given
    } yield (a, b)

    new Axiom(genAB, {case (a_, b) => f(a_).test(b)})
  }

  def map[B](f: A => B): Axiom[A, B] = new Axiom(gen, f)

  def foreach[ASSERTION](f: A => ASSERTION): Unit = forAll(gen)(f)

  def filter(p: A => Boolean): Given[A] = new Given(gen.filter(p))

  def withFilter(p: A => Boolean): Given[A] = filter(p)

}

object Given {

  def oneOf[A](s: Seq[A]): Given[A] = new Given(Gen.oneOf(s))

  def oneOf[A](a: A, b: A): Given[A] = new Given(Gen.oneOf(a, b))

  def const[A](a: A): Given[A] = new Given(Gen.const(a))

  def size: Given[Int] = new Given(Gen.size)

  def sized2: Given[(Int, Int)] = {
    for {
      m <- size
      n <- oneOf(0 to m)
      size2 <- oneOf((m, n), (n, m))
    } yield size2
  }

  def sized[A](f: Int => Given[A]): Given[A] = new Given[A](Gen.sized(i => f(i).gen))

  def sized2[A](f: (Int, Int) => A): Given[A] = {
    sized(n =>
      for {
        m <- oneOf(0, n)
        (i, j) <- oneOf((m, n), (n, m))
      } yield f(i, j)
    )
  }

  def pick[A](n: Int, i: Iterable[A]): Given[Seq[A]] = new Given(Gen.pick(n, i))

}
