package axiom

import org.scalatestplus.scalacheck.ScalaCheckDrivenPropertyChecks

trait AxiomDrivenPropertyChecks extends ScalaCheckDrivenPropertyChecks {
  final def axiom[GIVEN, ASSERTION](axiom: Axiom[GIVEN, ASSERTION]): Unit =
      forAll(axiom.given)(axiom.test)
}
